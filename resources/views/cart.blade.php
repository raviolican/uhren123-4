

<?php
    /*
     * Using modified layout for optimizing conversions!
     */
?>
@extends('layouts.appCart')

@section('content')
    <center>
    <h1 style="">CART</h1>
    </center>
    <div class="container">

        <div class="row">
            @if(isset($products) && count($products) >0 )
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Sum</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td class="product-in-table">
                                <img class="img-responsive" src="{{$product->thumbnail}}_SL200.jpg" alt="">
                                <div class="product-it-in"  style="max-width: 300px;">
                                    <p>
                                        {{$product->name}}
                                    </p>
                                </div>
                            </td>
                            <td>EUR  <i id="price_total">€ {{number_format($product->price/100,2, ",", ".")}}*</i></td>
                            <td>
                                <select class="form-control input-sm qtyChekc" id="qtySelect" asin="{{$product->asin}}">
                                    @for($i = 1; $i <= 10;$i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                            </td>
                            <td class="shop-red">{{number_format(($product->price/100)*(\Session::get("shopping_cart")[$product->asin]["qty"]),2, ",", ".")}}</td>
                            <td>
                                <div  asin="{{$product->asin}}" id="cart_delete" class="close">×</div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="coupon-code">
                    <div class="row">
                        <div class="col-sm-4 sm-margin-bottom-30">

                        </div>
                        <div class="col-sm-3 col-sm-offset-5">
                            <ul class="list-inline total-result">


                                <li class="divider"></li>
                                <li class="total-price">
                                    <h4>Total:</h4>
                                    <div class="total-result-in">
                                        @if(isset($subtotal))
                                            <span>€ {{number_format($subtotal/100,2, ".", ".")}}</span>
                                        @endif

                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>

                                    <a href="{{\Session::get("amz_pruchase_url")}}" target="_blank">
                                        <button type="button" id="redirectAMZ" class="btn btn-lg btn-primary">CHECKOUT</button>
                                    </a>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            @else
                <center>
                <img src="{{asset("images/no_products_added_yet.gif")}}" alt=""> <br> <br>
                    Here is nothing, yet.
                </center>
            @endif

                <hr>
                <div class="col-md-3">
                    <b>Why Us?</b>
                    <p>
                        - Absolutely no Registration Required. <br>
                        - Highly secure Shop-Ecosystem. <br>
                        - Extremely high product stocks. <br>
                        - Lowest prices on the market. <br>
                        - State-Of-Art refund policy.
                    </p>
                </div>

                <div class="col-md-3">
                    <b>About GAMEZZZ.</b>
                    <p>
                        GAMEZZZ, found in 2017 is a with Amazon affiliated shop ecosystem that finally made it
                        easy for customers to find cheapest products on the markets. Highly available servers make it to one of the most valuable shops worldwide.
                    </p>
                </div>

                <div class="col-md-3">
                    <b>Security at its glance</b>
                    <p>
                        We love your privacy and therefore we maintain a sophisticated secure enviroment to protect us and you from fraud, leaks and any other vulnerability on the internet.
                    </p>
                </div>

                <div class="col-md-3">
                    <b>Fast Shipping</b>
                    <p>
                        You can select from Digital download or get your Product shipped directly to your home with shortest delivery time by free-premium deliveries.
                    </p>
                </div>

        </div>
        <small style="font-size: x-small">Prices and availability might change on checkout slightly.
            <a href="{{url("/")}}"> <b>Return to index.</b></a></small>
    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            ajaxuse = false;


            $(".qtyChekc").on("change", function () {

                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('cart/add/')}}' + "/" + $(this).attr("asin") + "/" + $(this).val(),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            ajaxuse = false;
                            location.reload();
                        }
                    });
                    ajaxuse = false;
                }
            });

            $(".close").on("click", function () {
                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('cart/delete/')}}' + "/" + $(this).attr("asin"),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            ajaxuse = false;
                            location.reload();
                        }
                    });
                    ajaxuse = false;

                }

            });
        });

    </script>
@endsection