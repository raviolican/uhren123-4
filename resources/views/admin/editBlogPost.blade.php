@extends('layouts.app')
@section('header')
    <script src="{{asset("js/tinymce/tinymce.min.js")}}"></script>
    <script>tinymce.init({ selector: 'textarea',
            height: 500,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ] });</script>
@endsection
@section('footer')
    <script src="{{asset("js/codeeditor/codemirror.js")}}"></script>
    <link rel="stylesheet" href="{{asset("css/codeeditor/codemirror.css")}}">
    <script src="{{asset("js/codeeditor/mode/javascript/javascript.js")}}"></script>
    <script type="text/javascript">
        var area = document.getElementById('additional_js');
        var myCodeMirror = CodeMirror.fromTextArea(area, {
            lineNumbers: true
        });
    </script>
@endsection

@section('content')

    <div class="containter">
        @include("layouts.adminNavigation")

        <div class="col-md-7">
            @if (count($errors) > 0)
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger fade in">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            @if(\Illuminate\Support\Facades\Session::has("notification"))
                <div class="alert alert-info fade in">
                    {{(\Illuminate\Support\Facades\Session::get("notification"))}}
                </div>
            @endif
            <form action="" method="POST"  enctype="multipart/form-data" >
                {{csrf_field()}}
                <h1>Add a new Blogpost</h1>
                <fieldset>
                    <section>
                        <label for="title">Titel</label>
                        <input class="form-control" type="text" id="title" name="title" id="title" value="{{$article->title}}">
                    </section>
                    <br>
                    <section>
                        <label for="seoslug">Seo-Slug</label>
                        <input class="form-control" type="text" id="seoslug" name="seo_slug" id="slug" value="{{$article->seo_slug}}">
                    </section>
                    <br>
                    <section>
                        <label for="desc">Description</label>
                        <input class="form-control" type="text" id="desc"
                               name="description" placeholder="about 166 characters long" value="{{$article->description}}" >
                    </section>
                    <br>
                    <section>
                        <label for="image">Post-Image</label>
                        <input   type="file" name="image" id="image" value="{{$article->article_image}}">
                    </section>
                    <br>
                    <section>
                        <label for="image_org">Image Organization</label>
                        <input class="form-control" type="text" id="image_org" name="image_organization" placeholder="" value="{{$article->image_organization}}" >
                    </section>
                    <br>
                    <section>
                        <label for="image_credit">Image Credit</label>
                        <input class="form-control" type="text" name="image_credit" id="image_credit" placeholder="" value="{{$article->image_credit}}" >
                    </section>
                    <!---
                    <section>
                        <label for="image_cap">Image Caption</label>
                        <input  class="form-control" type="text" id="image_cap" name="image_caption" placeholder="" value="NOT_IMPLEMENTED" >
                    </section>
                    --->
                    <br>
                    <section>
                        <label for="article_txt">Article</label>
                        <textarea class="form-control" id="article_txt" rows="10" id="article" name="text">{!! $article->text!!}</textarea>
                    </section>
                    <br>
                    <section>
                        <label for="kwds">Keywords</label>
                        <input class="form-control" type="text" id="kwds" name="keywords" value="{{$article->keywords}}" >
                    </section>
                    <br>
                    <section>
                        <label for="cats">Category (current: "{{$article->category}}")</label>
                        <select name="category" id="cats" class="form-control">
                            <option value="news">News</option>
                            <option value="uhren_test">Uhren-Test</option>
                        </select>
                    </section>
                </fieldset>
                <br>
                <footer>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-primary"
                            onclick="window.history.back();">Back</button>
                </footer>
            </form>
        </div>

    </div>
@endsection