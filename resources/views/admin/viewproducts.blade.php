@extends('layouts.app')

@section('content')
    <div class="containter">
        @include("layouts.adminNavigation")
        <div class="col-md-9">
        @if(count($products)>0)

                <table class="table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>ASIN</th>

                        <th>Category</th>
                        <th>Price</th>
                        <th>Views</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products AS $prodcut)
                        <tr>
                            <td>{{$prodcut->id}}</td>
                            <td>{{substr($prodcut->name,0,46)}}</td>

                            <td><small>{{$prodcut->asin}}</small></td>
                            @if($prodcut->category == 0)
                                <td>Digitaluhren</td>
                            @elseif($prodcut->category == 1)
                                <td>Automatikuhren</td>
                            @elseif($prodcut->category == 2)
                                <td>Quarzuhren</td>
                            @endif
                            <td>€ {{number_format($prodcut->price/100,2, ",", ".")}}*</td>
                            <td>{{$prodcut->views}}</td>
                            <td>

                                @if($prodcut->featured == NULL)
                                    <button class="feature btn btn-xs btn-primary"
                                            value="{{$prodcut->id}}" setting="1"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></button>
                                @elseif($prodcut->featured == 1)
                                    <button class="feature btn btn-xs btn-primary"
                                            value="{{$prodcut->id}}" setting="0"><span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span></button>
                                @elseif($prodcut->featured == 0)
                                    <button class="feature btn btn-xs btn-primary"
                                            value="{{$prodcut->id}}" setting="1"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></button>
                                @endif

                                    <button class="delete btn btn-xs btn-danger" value="{{$prodcut->id}}"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @else
                @endif
        </p>

    </div>
@endsection

@section("footer")
            <script>
                $('document'). ready( function(){
                    ajaxuse = false;
                    $(".feature").on("click", function () {
                        featureitem = $(this);
                        if(ajaxuse == true)
                        {
                            return;
                        } else {
                            ajaxuse = true;
                            $.ajax({
                                url: '{{url('admin/products/feature/')}}'+"/"+featureitem.val()+"/"+featureitem.attr("setting"),
                                type: 'POST',
                                data:{
                                    '_token' : '{{csrf_token()}}'
                                },
                                success: function(result){
                                    if(result == 1)
                                    {
                                        featureitem.attr("setting", "0");
                                        featureitem.html("<span class=\"glyphicon glyphicon-star-empty\" aria-hidden=\"true\"></span>")

                                    }
                                    else if(result == 0)
                                    {
                                        featureitem.attr("setting", "1")
                                        featureitem.html("<span class=\"glyphicon glyphicon-star\" aria-hidden=\"true\"></span>")
                                    }
                                }
                            });
                            ajaxuse = false;
                        }

                    });
                    $(".delete").on("click", function () {
                        delitem = $(this);
                        if(ajaxuse == true)
                        {
                            return;
                        } else {
                            ajaxuse = true;
                            $.ajax({
                                url: '{{url('admin/products/delete/')}}'+"/"+$(this).val(),
                                type: 'POST',
                                data:{
                                    '_token' : '{{csrf_token()}}'
                                },
                                success: function(result){
                                    if(result == "Success")
                                    {
                                        delitem.parent().parent().fadeOut( "slow" ).remove();
                                    }
                                    else
                                    {
                                        alert('Error while trying to delete product<br>'.result);

                                    }
                                }
                            });
                            ajaxuse = false;
                        }
                    });
                });
            </script>
@endsection





