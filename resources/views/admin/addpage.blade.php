@extends('layouts.app')

@section('content')

    <!--=== End Breadcrumbs ===-->
    <div class="container content">
        @if(Session::has("notification"))
            <div class="alert alert-success fade in">
                {{Session::get("notification")}}
            </div>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger fade in">
                    {{ $error }}
                </div>
            @endforeach
        @endif
        @include("layouts.adminNavigation")


        <div class="col-md-9">
            <form action="" method="post">
                <h1>Add a new page</h1>
                {{csrf_field()}}
                <section>
                    <label for="title">Title</label>
                    <input class="form-control" type="text" name="title" placeholder="Title" required>
                </section>

                <section>
                    <label for="slug">
                        SEO-Slug - <i>Enter a fancy URL here for your SEO strategy</i></label>
                    <input class="form-control" type="text" name="slug" value="NOT_IMPLEMENTED!">
                </section>

                <section>
                    <label for="description">Description</label>
                    <input class="form-control" type="text"
                           name="description" placeholder="A Description" required>
                </section>

                <section>
                    <label for="html">HTML</label>
                    <textarea class="form-control" name="html" cols="80" rows="10"
                              placeholder="Some html">
                        <p>Hello, World!</p>
                    </textarea>
                </section>

                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button"  class="btn  btn-danger delete_site">Delete</button>
            </form>
        </div>
    </div>
@endsection


@section("footer")

@stop