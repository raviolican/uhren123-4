<?php
/*
 * Todo: Implement EditCategory
 */
?>

@extends('layouts.app')

@section('content')
    <div class="containter">
        @if(Session::has("notification"))
            <div class="alert alert-success fade in">
                {{Session::get("notification")}}
            </div>
        @endif
        @if (count($errors) > 0)
            <div class="alert alert-danger fade in">
                Errors: <br>
                @foreach ($errors->all() as $error)
                    - {{ $error }} <br>
                @endforeach
            </div>
        @endif

        @include("layouts.adminNavigation")
        <div class="col-md-9">
            <div>
                <fieldset>
                    <section>
                        @if(count($slides) > 0)
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Image</th>
                                    <th>Caption</th>
                                    <th>Active</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($slides  AS $slide)
                                    <tr slideId="{{$slide->id}}">
                                        <td> {{$slide->id}}</td>
                                        <td> {{$slide->image}}</td>
                                        <td> {{$slide->caption}}</td>

                                        <td>
                                            @if($slide->active == 1)
                                                Yes
                                            @else
                                                No
                                            @endif
                                        </td>

                                        <td slideId="{{$slide->id}}" >
                                            <button type="button" id="editbtn"
                                                    class="btn btn-xs btn-primary editbtn" >
                                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                            </button>
                                            @if($slide->active)
                                                <button type="button" id="editbtn"
                                                        class="btn btn-xs btn-primary activateButton" >
                                                    Deactivate
                                                </button>
                                            @else
                                                <button type="button" id="editbtn"
                                                        class="btn btn-xs btn-primary activateButton" >
                                                    Activate
                                                </button>
                                            @endif

                                            <button type="button" id="deletebtn"
                                                    class="btn btn-xs btn-danger deletebtn">
                                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                            </button>
                                    </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        @else
                            <p>
                                No slides were found.
                            </p>
                        @endif
                    </section>
                </fieldset>
            </div>
            <br>
            <div id="editSlide" hidden="true">
                <h3>Edit</h3>
                <form action=""method="post">
                    <input type="hidden" name="_method" value="PATCH">
                    <input type="hidden" id="slideID" name="slideID" value="">

                    {{csrf_field()}}

                    <div class="form-group">
                        <label for="caption">Caption</label>
                        <input type="text" name="caption" class="form-control" id="caption" aria-describedby="captionHelp"/>
                        <small id="captionHelp" class="form-text text-muted"> </small>
                    </div>
                    <footer>
                        <button type="submit" class="btn-u">Update</button>
                        <button type="button" id="cancleEdit" class="btn-u">Cancle</button>
                    </footer>
                </form>

            </div>
            <h2>Create a new slider</h2>
            <form action="" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PUT">
                {{csrf_field()}}

                <div class="form-group">
                    <label for="caption">Image</label>
                    <input name="image" type="file"  aria-describedby="imageHelp">
                    <small id="imageHelp" class="form-text text-muted"> </small>
                </div>


                <div class="form-group">
                    <label for="caption">Caption</label>
                    <input type="text" name="caption" class="form-control" id="caption" aria-describedby="captionHelp"/>
                    <small id="captionHelp" class="form-text text-muted"> </small>
                </div>

                <footer>
                    <button type="submit" class="btn-u">Add</button>
                </footer>
            </form>
        </div>

    </div>
@endsection

@section("footer")
    <script>
        $('document'). ready( function(){

            $(".editbtn").on("click", function () {
                $("#editSlide").fadeIn();
                var f = $(this).parent().attr("slideId");

                $("#editSlide #slideID").attr("value",f);
            })
            $("#cancleEdit").on("click", function () {
                $("#editSlide").fadeOut();
            })

            $(".activateButton").on("click", function () {
                $item = $(this);
                $ajaxInUse = true;
                $.ajax({
                    url: '/admin/slides',
                    type: "POST",
                    data: {
                        '_token': '{{csrf_token()}}',
                        '_method': 'PATCH',
                        'slideID': $item.parent().attr("slideId")
                    },

                    success: function(result) {
                       location.reload();
                    },

                    error: function(result) {
                        alert("Error!");

                    }
                });

            });


            $(".deletebtn").on("click", function () {
                $item = $(this);
                $ajaxInUse = true;
                $.ajax({
                    url: '/admin/slides',
                    type: "POST",
                    data: {
                        '_token': '{{csrf_token()}}',
                        '_method': 'DELETE',
                        'slideID': $item.parent().attr("slideId")
                    },

                    success: function(result) {
                        location.reload();
                    },

                    error: function(result) {

                    }
                });

            });
        });
    </script>
@endsection