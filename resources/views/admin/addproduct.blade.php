@extends('layouts.app')

@section('content')

    <div class="containter">

        @include("layouts.adminNavigation")

        <div class="col-md-9">
            @if (count($errors) > 0)
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger fade in">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            @if(\Illuminate\Support\Facades\Session::has("notification"))
                <div class="alert alert-info fade in">
                    {{(\Illuminate\Support\Facades\Session::get("notification"))}}
                </div>
            @endif

            <form action="{{route('addNewProduct')}}" method="POST">
                {{csrf_field()}}
                <h2> Add a new Product </h2>

                <section>
                    <label class="label">Asin</label>
                    <label class="input">
                        <input type="text" name="asin">
                    </label>
                </section>

                @if(count($fetchCats) > 0)
                <section>
                    <select class="cSelector" name="categories[]" style="height: 100%"  multiple>
                        @foreach($fetchCats as $cat)
                            @if($cat->parent_id == 0)
                                <option value="{{$cat->id}}">-{{$cat->name}}</option>
                                @foreach($cat->children as $child)
                                    <option value="{{$child->id}}">--{{$child->name}}</option>
                                @endforeach
                            @else
                                @continue
                            @endif
                        @endforeach
                    </select>
                </section>
                @endif

                <section>
                    <button type="submit" class="btn btn-u">Submit</button>
                    <button type="button" class="btn btn-u btn-u-default" onclick="window.history.back();">Back</button>
                </section>

            </form>

        </div>

    </div>
@endsection