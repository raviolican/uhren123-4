@extends('layouts.app')
@section('footer')
    <script src="{{asset("js/codeeditor/codemirror.js")}}"></script>
    <link rel="stylesheet" href="{{asset("css/codeeditor/codemirror.css")}}">
    <script src="{{asset("js/codeeditor/mode/javascript/javascript.js")}}"></script>
    <script type="text/javascript">
        var area = document.getElementById('additional_js');
        var myCodeMirror = CodeMirror.fromTextArea(area, {
            lineNumbers: true
        });
    </script>
@endsection

@section('content')

    <div class="containter">
        @include("layouts.adminNavigation")

        <div class="col-md-7">
            @if (count($errors) > 0)
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger fade in">
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            <h2> Google Settings </h2>
            <p>
                If you want to add the site-verification tag, goto <i>General</i> -> <i>Additional HTML inside Header</i>.
            </p>
            <form action="/admin/site/meta" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="g_analytics_code">Analytics Snippet</label>
                    <textarea type="text" name="g_analytics_code" value="{{$settings["title"]}}" class="form-control"
                              id="inputTitle" aria-describedby="g_analytics_codeHelp">{{$settings["g_analytics_code"]}}</textarea>
                    <small id="g_analytics_codeHelp" class="form-text text-muted">Copy your analytics snippet and paste it here. It will
                    appear on every page at the right place except admin area.</small>
                </div>

                <section>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </section>

            </form>

        </div>

    </div>
@endsection