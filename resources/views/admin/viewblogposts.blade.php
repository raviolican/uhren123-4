@extends('layouts.app')
@section('content')
    <div class="container content">
        @include("layouts.adminNavigation")

        <div class="col-md-9">
        @if(count($articles)>0)
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Titel</th>
                    <th>Category</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                @foreach($articles AS $article)
                    <tr>
                        <td>{{$article->id}}</td>
                        <td>
                            {{$article->title}}
                        </td>
                        <td>
                            {{$article->category}}
                        </td>
                        <td>
                            <a href="./update/{{$article->id}}" class="editArticle"
                               id="editArticle" articleId="{{$article->id}}">Edit Article</a>
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
            <div id="editorContainer" hidden>
                <button type="button" class="btn-u btn-u-default" id="backbutton">Back</button>
                <div id="editor"></div>
            </div>
        @else
            <div class="alert alert-info fade in">
                Oh, there are no articles yet.
            </div>
        @endif
        {!! $articles->render() !!}
    </div>

</div>

@endsection