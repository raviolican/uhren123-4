@extends('layouts.app')

@section('header')
    <title>{{$meta["title"]}}</title>
    <meta name="description" content="{{$meta["description"]}}}}">
    <link rel="stylesheet" href="{{asset("css/owl-carousel/assets/owl.carousel.min.css")}}">
    <style>
        .ui.buttons .or::before {
            content: '?' !important;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.js"></script>
@endsection

@section('content')
    <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "Product",
      "name": "{{$productArray["name"]}}",
      "image": "{{$productArray["thumbnail"]}}.jpg",
      "description": "{{strip_tags($productArray["editorreview"])}}",
      "mpn": "925872",
      "brand": {
        "@type": "Thing",
        "name": "{{$productArray["brand"]}}"
      },
      "offers": {
        "@type": "Offer",
        "priceCurrency": "EUR",
        "price": "{{number_format( ($productArray["price"]) / 100,2, ".", ".")}}",
        "priceValidUntil": "2025-11-05",
        "itemCondition": "http://schema.org/NewCondition",
        "availability": "http://schema.org/InStock"
      }
    }
    </script>
    <div class="container">
        <h1 class="ui header">{{$productArray["name"]}}</h1>
        <div class="col-md-5">
            <ul class="bxslider" >
                <li><img src="{{$productArray["thumbnail"]}}.jpg" alt="Main Product Image"></li>
                @foreach($productArray["imgurls"] AS $imgurl)
                    <li><img src="{{$imgurl}}.jpg"  alt="another image of{{$productArray["name"]}}"></li>
                @endforeach
            </ul>
            <div id="bx-pager">
                <?php $x = 0; ?>
                    <a data-slide-index="{{++$x}}" href="#">
                        <img src="{{$productArray["thumbnail"]}}jpg" alt="thumbnail of the main image" width="50" hidden="hidden"/>
                    </a>
                @foreach($productArray["imgurls"] AS $imgurl)
                    <a data-slide-index="{{$x}}" href="#">
                        <img src="{{$imgurl}}jpg" alt="another thumbnail of the main image from the product {{$productArray["name"]}}" width="50" />
                    </a>
                    <?php $x++; ?>
                @endforeach
            </div>
        </div>
            <div class="col-md-7">
                <div class="ui list">
                    @foreach($productArray["features"] as $feat)
                        <span class="item">
                            <i class="right triangle icon"></i>
                            <div class="content">
                                <div class="description">{{$feat}}</div>
                            </div>
                        </span>
                    @endforeach
                </div>
                <h4 class="ui horizontal divider header">
                    <i class="tag icon"></i>
                    Jetzt Kaufen
                </h4>
                <span  class="productPrice">
                    € {{number_format( ($productArray["price"]) / 100,2, ".", ".")}}
                </span>
                <br><br>


                <div class="ui labeled input">
                    <div class="ui label">
                        Anzahl
                    </div>
                    <input placeholder="mysite.com" id="qty"  value="1" type="number">
                </div>

                <div class="ui buttons" style="float: right">
                    <button class="ui button">Sofortkauf</button>
                    <div class="or">ds</div>
                    <button class="ui positive button" id="addcart">In den Einkaufswagen</button>
                </div>

                <!---
                <button style="float: right" type="button" id="addcart" class="btn btn-success">
                    ADD TO CART
                </button>
                --->
                <br> <br>
            </div>

            <h4 class="ui horizontal divider header">
                Produkt Video
            </h4>
            <center>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/8I2RKg20z1Q?autoplay=0&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
            </center>
    </div>
    <h4 class="ui horizontal divider header">

        Nähere Informationen
    </h4>
    <table class="ui definition table">
        <thead>
        </thead>
        <tbody>
        @foreach($productArray["attributes"] as $k => $v)
            @if($k == "Feature")
                @continue
            @elseif($k == "ItemDimensions")
                @continue
            @elseif($k == "main_category")
                @continue
            @elseif($k =="UPCList")
                @continue
            @elseif($k== "ListPrice")
                @continue
            @elseif($k== "EANList")
                @continue
            @elseif($k=="PackageDimensions")
                @continue
            @elseif($k=="CatalogNumberList")
                @continue
            @elseif($k == "Languages")
                @continue
            @elseif($k == "Label")
                @continue
            @elseif($k == "PartNumber")
                @continue
            @elseif($k == "ProductTypeName")
                @continue
            @endif

            <tr>
                @if($k == "Binding")
                    <td>Produkt Art</td>
                @elseif($k == "Brand" || $k == "Studio")
                    <td>Marke</td>
                @elseif($k == "HardwarePlatform")
                    <td>Plattform</td>
                @elseif($k == "PublicationDate" || $k == "ReleaseDate")
                    <td>Veröffentlicht</td>
                @elseif($k == "IsAdultProduct")
                    <td>Freigegeben Aab 18 Jahren</td>
                    <td>{{($v == 0 ? "Nein" : "Ja")}}</td>
                    @continue
                @elseif($k == "Title")
                    <td>Titel</td>
                @elseif($k == "LegalDisclaimer")
                    <td>Hinweis</td>
                @elseif($k == "Manufacturer"  || $k == "Publisher")
                    <td>Hinweis</td>
                @else
                    <td>{{$k}}</td>
                @endif
                <td>{{$v}}</td>
            </tr>
        @endforeach
        </tbody></table>
    <h4 class="ui horizontal divider header">
        Produkte die Du lieben wirst
    </h4>


        <div class="ui five cards" style="">
            @foreach($recommendations as $latest)
            <div class="card">
                <div class="image">

                        <img src="{{$latest->thumbnail}}_SL250_.jpg">

                </div>
                <div class="content">
                    <div class="header">{{$latest->name}}</div>
                    <div class="meta">
                    </div>
                    <div class="description">
                        € {{number_format($latest->price/100,2, ",", ".")}}
                    </div>
                </div>
                <div class="extra content">
                    <a href="{{url("shop/".$latest->seo_slug."/".$latest->id)}}">
                    <button class="ui button">Zum Product</button>
                    </a>
                </div>
            </div>
         @endforeach

        </div>
    <div class="ui five cards">

    </div>
    <img src="" alt="">


@endsection
@section('footer')
    <script src="{{asset('/js/slider/jquery.bxslider.min.js')}}"></script>
    <!-- bxSlider CSS file -->
    <link href="{{asset('/css/slider/jquery.bxslider.css')}}" rel="stylesheet" />
    <script src="{{asset("js/owl-slider/owl.carousel.min.js")}}"></script>
    <script>
        jQuery(document).ready(function() {
            ajaxuse = false;
            cardadded = false;
            var mx = true;
            $("#addcart").on("click", function () {
                var my = $(this);
                if(ajaxuse == true) {
                    return;
                } else {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('cart/add/')}}' + "/{{$productArray['asin']}}/" + $("#qty").val(),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            cardadded = true;
                            if(mx) {
                                my.after("<br><br>");
                                mx=false;
                            }
                            my.parent().parent().after("<div style='width: 300px;float: right; position: absolute;top: 100px; right: 0px' class='alert alert-info'>"+result+"</div>");
                           // location.reload();
                            $(".alert").fadeOut(5000);
                        },
                        error: function (result) {
                            my.parent().parent().after("<div style='width: 300px;float: right; position: absolute;top: 100px; right: 0px' class='alert alert-danger'>Error occurred. Please try again or contact admin.</div>");
                            $(".alert").fadeOut(5000);
                        }
                    });
                    ajaxuse = false;
                }
            })
        });
        $(".owl-carousel").owlCarousel({
            navigation: false, // Show next and prev buttons
            // autoWidth:true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            transitionStyle: "fadeUp",
            autoPlay: true,
        });
        $('.bxslider').bxSlider({
            pagerCustom: '#bx-pager'
        });
        var slideIndex = 1;
        function plusDivs(n) {showDivs(slideIndex += n);}
        function currentDiv(n) {showDivs(slideIndex = n);}
    </script>
@endsection
