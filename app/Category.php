<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = array(
        "name", "description", "parent_id"
    );
    /*
     * The Category can have many products
     */
    public function products()
    {
        return $this->belongsToMany('App\Product')
            ->withTimestamps();
    }

    public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }
}
