<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Amazon\ProductParser;
use App\Product;
use App\Settings;
use App\Site;
use App\Slider;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\HttpException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Category;

class AdminController extends Controller
{

    public function storeNewProduct(Request $request)
    {
        $variations = false;

        /* Amazon ASINS do have a legnth of 10 chars currently, change if updated */
        $this->validate($request, array(
           'asin' => 'required|min:10|max:10'
        ));

        // Check if product already is in Database
        $response = Product::where('asin', '=', $request->asin)->get();
        if(count($response) >= 1)
            return redirect()->back()->withErrors('Product already in database.');

        $amazon = new Amazon\Amazon();

        if(FALSE == ($response = $amazon->searchProductByASIN($request->get('asin'))))
            return redirect()->back()->withErrors('Could not connect or product was not found.');
        // New reference
        $product = &$response;

        // TODO: Make it work with more variations!
        if($amazon->productHasVariation())
        {
            $variations = true;
            try {
                if($response->Items->Item->Variations->TotalVariations > 1)
                    throw new HttpException("Troubles while retrieving product variations!");
            } catch (HttpException $e) {
                return redirect()->back()->withErrors($e);
            }
        }

        // Nasty ~.~ Parsing
        $parser = new ProductParser($product, $variations);
        try {
            $product = $parser->parse(); /* FYI: parse() returns new Product Object. */
        } catch (QueryException $e) {
            return redirect()->back()->withErrors("Errors while parsing AmazoXML response.");
        }

        $product->save();
        $product->categories()->attach($request->categories);
        Cache::forget("products");
        $request->session()->flash("notification", "Product added successfully!");
        return redirect()->back();
        //
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function viewProducts()
    {
        $products = Product::orderBy('views', 'DESC')->paginate(60);
        return view('admin.viewproducts', compact('products'));
    }


    /**
     * @param $id
     * @return \Exception|QueryException|string
     */
    public function removeProduct($id)
    {
        try{
            $product = Product::where("id", $id)->first();
            $product->delete();
            Cache::forget("products");
            return "Success";
        } catch ( \Illuminate\Database\QueryException $e ) {
            return $e;
        }
    }


    public function patchSiteSettings(Request $request)
    {
        /* Todo: Make it that it validates dynamically.
        $this->validate($request, [
            "title" => "required",
            "description" => "required",
        ]);
        */
        foreach ($request->except("_token") as $k => $v) {

            $t = Settings::where("key", $k)->first();
            $t->value = $v;
            $t->save();
        }
        Cache::forget("settings");
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function editSettings(Request $request)
    {
        try {
            $settings = \App\Settings::where("id", 1)->get()->first();
            $settings->update($request->all());
            $request->session()->flash("notification", "Meta updates successfully!");
            return redirect()->back();

        } catch (\PDOException $e) {
            return redirect()->back()->withErrors('Error while trying to update settings.');
        }
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function addCategory(Request $request)
    {
        // Validate inputs
        $this->validate($request,[
            "name" => "required",
            "description" => "required",
            "parent_id" => "required|numeric"
        ]);

        try{
            // Check if category already exists in DB
            $categoryCheck = Category::where('name', $request->name)->first();

            if(is_null($categoryCheck)) {
                // Add the category
                $newCategory = new Category($request->all());
                if($newCategory->save()){


                    $request->session()->flash("notification", "Category has been added.");
                    Cache::forget("categories");
                } else {
                    throw new \Exception("Error while trying to save category in DB!");
                }
                return redirect()->back();
            } else {
                // Exists
                throw new \Exception("Category allready exists!");
            }
        } catch (\Exception $e) {
            // Return and show an error message
            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }

    /**
     * @param $id
     * @return array|string
     */
    public function deleteCategory( $id)
    {
        // Let's get the category!
        $dCategory = Category::find($id);
        if(is_null($dCategory)) {
            return "1"; // Category not found or already deleted
        } else {
            // Let's see if category has children
            if(count($dCategory->children) > 0)
                return array(
                    "code" => "0",
                    "msg"  => "Please delete children first."
                );
            else
                return array(
                    "code" => (string)($dCategory->delete()),
                    "msg"  => ""
                ); // No children, so delete the category.
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function updatePage(Request $request, $id)
    {
        $this->validate($request,[
            "title" => "required",
            "description" => "required",
            "html" => "required",
            "slug" => "required",
        ]);

        $pPage = Site::find($id);

        $pPage->title       = $request->title;
        $pPage->description = $request->description;
        $pPage->html        = $request->html;

        if($pPage->save()) {
            $request->session()->flash("notification", "Page updated!");
            return redirect()->back();
        } else {
            return redirect()->back()
                ->withErrors('Error while trying to update page.');
        }
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function addPage(Request $request)
    {
        $this->validate($request,[
            "title" => "required",
            "description" => "required",
            "html" => "required",
            "slug" => "required",
        ]);

        $pPage = new Site($request->all());

        if($pPage->save()) {
            $request->session()->flash("notification", "Page created!");
            return redirect()->back();
        } else {
            return redirect()->back()
                ->withErrors('Error while trying to save new page.');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function putSlider(Request $request)
    {
        // Todo: Validation

        //dd($request->file("image"));
        $path = $request->file("image")->store("slides"); /* Only Supports JPG */

        $slide = new Slider();
        $slide->image   = $path;
        $slide->caption = $request->caption;
        $slide->active  = 1;

        if($slide->save()) {
            $this->forgetCache("slides");
            $request->session()->flash("notification", "Slide saved successfully!");
            return redirect()->back();
        } else {
            $request->session()->flash("error", "Error while trying to save file.");
            return redirect()->back();
        }
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function deleteSlider(Request $request)
    {
        $slider = Slider::find($request->slideID);
        if( $slider->delete())
        {
            $this->forgetCache("slides");
            return redirect()->back();
        } else {
            return redirect()->back()->withErrors("Couldn't delete Slide!");
        }

    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function patchSlider(Request $request)
    {

        $slide = Slider::find($request->slideID);

        if($slide == null) {
            return redirect()->back()->withErrors("Could not find requested ID");
        } else {

            if(isset($request->caption)) {
                $slide->caption = $request->caption;
            } else {

                $slide->active  = $slide->active == 1 ? 0 : 1;
            }

            if($slide->update()) {
                $this->forgetCache("slides");
                $request->session()->flash("notification", "Slide updated.");
                return redirect()->back();
            } else {
                return redirect()->back()->withErrors("Could not save file.");
            }
        }
    }

    /**
     * Admin Configs Sliders
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function renderSlidesPage()
    {
        $slides = Slider::all();
        return view("admin.slider", compact("slides"));
    }

    /**
     * Clears Cache forg given $name
     * @param $name
     */
    private function forgetCache($name) {
        Cache::forget($name);
    }
}
