<?php

namespace App\Http\Controllers;

use App\Article;

class BlogController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $articles = Article::orderBy("created_at","desc")->paginate(6);
        $latest = Article::latest();
        return view("blog.blogindex", compact("articles", "latest"));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewArticle($id) {
        $article = Article::where("id", $id)->get()->first();
        //$imgsize = getimagesize(url($article->article_image));
        return view("blog.viewArticle", compact("article"));
    }
}
