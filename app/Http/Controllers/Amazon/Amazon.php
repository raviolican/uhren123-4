<?php

namespace App\Http\Controllers\Amazon;

use ApaiIO\ApaiIO;
use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\Operations\CartAdd;
use ApaiIO\Operations\CartCreate;
use ApaiIO\Operations\CartModify;
use ApaiIO\Operations\Lookup;
use ApaiIO\Request\GuzzleRequest;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\Translation\Exception\InvalidResourceException;

class Amazon
{
    private $configuration;
    private $client;
    private $request;
    private $apaiIO;
    private $xml;

    public function __construct()
    {
        $this->configuration = new GenericConfiguration();
        $this->client        = new Client();
        $this->client        = new Client();
        $this->client        = new Client();
        $this->request       = new GuzzleRequest($this->client);

        $this->apaiIO = new ApaiIO($this->compileConfiguration());
    }

    /**
     * @return GenericConfiguration
     */
    private function compileConfiguration()
    {
         /* Load data from Cache */
        $t = Cache::get("settings");

        $this->configuration
            ->setCountry($t["a_country"])
            ->setAccessKey($t["a_c_key_id"])
            ->setSecretKey($t["a_c_key_secret"])
            ->setAssociateTag($t["a_associateTag"])
            ->setRequest($this->request);

        return $this->configuration;
    }

    /**
     * @param $asin <string> asin of product
     * @param $condition <string> condition (Default 'All')
     * @return bool|\SimpleXMLElement
     */
    public function searchProductByASIN($asin, $condition = 'New')
    {
        $search = new Lookup();
        $search->setCondition("All");
        $search->setItemId($asin);

        $search->setResponseGroup(array(
            'Images', 'Large', 'VariationImages', 'Variations', 'OfferFull'
        ));

        $response = $this->apaiIO->runOperation($search);

        $this->xml = simplexml_load_string($response);
       // dd($this->xml);
        if (isset($this->xml->Item->Request->Errors->Error->Message))
            return false;
        else
            return $this->xml;
    }

    /**
     * @param $asin
     * @param $quantity
     * @return \SimpleXMLElement
     * @throws InvalidResourceException
     */
    public function createCart($asin, $quantity)
    {
        $cart = new CartCreate();
        $cart->addItem($asin, $quantity);

        $response = $this->apaiIO->runOperation($cart);

        $xml = simplexml_load_string($response);

        if(isset($xml->Cart->Request->Errors->Error)) {
            throw new InvalidResourceException();
        }
        return $xml;

    }

    /**
     * @param $cartItemID
     * @param $cartID
     * @param $HMAC
     * @param $qty
     * @throws InvalidResourceException
     */
    public function modifyCart_updateQty($cartItemID, $cartID, $HMAC, $qty)
    {
        // If product already in cart, we're going to update it's quantity!
        $cartModify = new CartModify();
        $cartModify->setCartId($cartID);
        $cartModify->setHMAC($HMAC);
        $cartModify->modifyQuantity($cartItemID, $qty);

        $retval = $this->apaiIO->runOperation($cartModify);

        $retval =  simplexml_load_string($retval);
        if(isset($retval->Cart->Request->Errors->Error)){

            if($retval->Cart->Request->Errors->Error->Code == "AWS.ECommerceService.ItemNotEligibleForCart")
            {
                throw new InvalidResourceException();
            }

        }
        return $retval;

    }

    /**
     * @param $cartID
     * @param $HMAC
     * @param $asin
     * @param $quantity
     * @throws InvalidResourceException
     */
    public function modifyCart_addCart($cartID,$HMAC,$asin,$quantity)
    {
        $addCart =  new CartAdd();
        $addCart->setCartId($cartID);
        $addCart->setHMAC($HMAC);
        $addCart->addItem($asin, $quantity);

        $retval = $this->apaiIO->runOperation($addCart);

        $retval =  simplexml_load_string($retval);

        if(isset($retval->Cart->Request->Errors->Error)){

            if($retval->Cart->Request->Errors->Error->Code
                == "AWS.ECommerceService.ItemNotEligibleForCart")
            {
               throw new InvalidResourceException();
            }

        }
        return $retval;

    }



    /**
     * @return bool
     */
    public function productHasVariation()
    {
        return isset($this->xml->Items->Item->Variations);
    }

    // Todo: implement this
    public function updatePrice()
    {

    }


}
