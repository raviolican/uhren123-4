<?php

namespace App\Http\ViewComposers;
use App\Category;
use App\Site;
use App\Http\Controllers\ShoppingCartController;
use Illuminate\View\View;
use App\Settings;
use phpDocumentor\Reflection\Types\Null_;

/**
 *
 */
class SettingsComposer
{
    /**
     * @param View $view
     */
    public function compose(View $view)
    {

        // Caching for site Settings.
        $view->with("settings", \Cache::remember('settings', 365, function() {
            return Settings::pluck('value','key');
        }));

        // Todo: Fix this
        $view->with("categories".  \Cache::remember('categories', 10, function() {
                return Category::all();
            }));
        $view->with("categories",Category::all() );

        // Get PAGES
        $view->with("pages", \Cache::remember('pages', 10, function() {

            return Site::all();
        }));
        $view->with("pages",Site::all() );



        $products = ShoppingCartController::getCartProducts();
        if($products != NULL) {
            $sessionProducts = \Request::session()->get("shopping_cart");


            // Calculate SUBTOTAL!
            $subtotal = 0;
            $products->map(function ($price) use (&$subtotal, $sessionProducts) {
                $subtotal = $subtotal + ($price->price * $sessionProducts[$price->asin]["qty"]);
            });

            // Counting the stuff
            if (\Request::session()->has("shopping_cart")) {
                $view->with("shopping_count", count($sessionProducts));
                $view->with("cart_items", $products);
                $view->with("subtotal", $subtotal);
            } else {
                $view->with("shopping_count", "");
            }
        }
    }
}