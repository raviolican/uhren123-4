<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = array(
        "key","value"
    );

    protected $guarded  = array('_method', '_token');

    protected $primaryKey = "key";
}
