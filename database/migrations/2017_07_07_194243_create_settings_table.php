<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key')->index();
            $table->string('value')->index()->nullable();
            $table->timestamps();
        });

        /* Pre-Configuration */
        /*
         * Dont remove pairs.
         */
        DB::table('settings')->insert(
            array(
                ["key" => "title", "value" => "MySimpleShop"],
                ["key" => "description", "value" => "An advanced Amazon Affiliate Site!"],
                ["key" => "keywords", "value" => "Site, Affiliate, Amazon, Advanced"],
                ["key" => "robots", "value" => "index,follow"],
                ["key" => "copyright", "value" => "(C) 2017 MySimpleShop. All rights reserved "],
                ["key" => "revisit_after", "value" => "1 days"],
                ["key" => "g_analytics_code", "value" => NULL],
                ["key" => "g_site_verify_code", "value" => NULL],
                ["key" => "a_c_key_id", "value" => NULL],
                ["key" => "a_country", "value" => NULL],
                ["key" => "a_associateTag", "value" => NULL],
                ["key" => "a_c_key_secret", "value" => NULL],
                ["key" => "additional_js", "value" => NULL],
            )
        );

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Settings');
    }
}
